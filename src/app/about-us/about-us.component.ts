import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-about-us',
  templateUrl: './about-us.component.html',
  styleUrls: ['./about-us.component.css']
})
export class AboutUsComponent implements OnInit {

  constructor() { }
  items = [
    {
      id : '01',
      text : 'Lorem ipsum dolor sit amet, consectetur  dolore magna aliqua.adipiscing elit,  dolore magna aliqua.',
      color : '#0AD1B9'
    },
    {
      id : '02',
      text : 'Lorem ipsum dolor sit amet, consectetur  dolore magna aliqua.adipiscing elit,  dolore magna aliqua.',
      color : '#0AD1B9'
    },
    {
      id : '03',
      text : 'Lorem ipsum dolor sit amet, consectetur  dolore magna aliqua.adipiscing elit,  dolore magna aliqua.',
      color : '#0AD1B9'
    }
  ];
  // tslint:disable-next-line:typedef

  ngOnInit(): void {
  }

}
